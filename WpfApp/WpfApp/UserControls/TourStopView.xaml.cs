﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp.Models;

namespace WpfApp.UserControls
{
    /// <summary>
    /// Interaction logic for TourStopView.xaml
    /// </summary>
    public partial class TourStopView : UserControl
    {
        private RoutedEventArgs a = null;

        public TourStopView()
        {
            InitializeComponent();
            ToursListBox.ItemsSource = WpfApp.Models.TourSource.GetAllTourStops(); ;
        }

        private void CalcButton_Click(object sender, RoutedEventArgs e)
        {
            var q = from stops in TourSource.GetAllTourStops()
                    where stops.Selected == true
                    select stops.EstimatedMinutes;


            MessegeTextBlock.Text = String.Format("{0} minutes", q.Sum());
            
        }

        private void ToursListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }
        
    }
}
